
BasicGame.Intro = function (game) {

    //  When a State is added to Phaser it automatically has the following properties set on it, even if they already exist:

    this.game;      //  a reference to the currently running game (Phaser.Game)
    this.add;       //  used to add sprites, text, groups, etc (Phaser.GameObjectFactory)
    this.camera;    //  a reference to the game camera (Phaser.Camera)
    this.cache;     //  the game cache (Phaser.Cache)
    this.input;     //  the global input manager. You can access this.input.keyboard, this.input.mouse, as well from it. (Phaser.Input)
    this.load;      //  for preloading assets (Phaser.Loader)
    this.math;      //  lots of useful common math operations (Phaser.Math)
    this.sound;     //  the sound manager - add a sound, play one, set-up markers, etc (Phaser.SoundManager)
    this.stage;     //  the game stage (Phaser.Stage)
    this.time;      //  the clock (Phaser.Time)
    this.tweens;    //  the tween manager (Phaser.TweenManager)
    this.state;     //  the state manager (Phaser.StateManager)
    this.world;     //  the game world (Phaser.World)
    this.particles; //  the particle manager (Phaser.Particles)
    this.physics;   //  the physics manager (Phaser.Physics)
    this.rnd;       //  the repeatable random number generator (Phaser.RandomDataGenerator)

    //  You can use any of these from any function within this State.
    //  But do consider them as being 'reserved words', i.e. don't create a property for your own game called "world" or you'll over-write the world reference.

    this.dialogue = ["Click to go to the next sentence.", 
        "Look at what I'm doing instead of\n studying for exam.",
        "But umm... Study hard, play hard?",
        "Lol ok, I need to study now.\n Ciao!"];
    this.mouseTouchDown = false;
    this.mouseTouchUp = false;
    this.i = 0;
    this.j = 0;
    this.timeCheck = 0;
    this.style = { font: "20px Verdana", fill: "#fff"};

    this.music = null;
    this.flag = 0;
    this.dummyVar = 0;


};

BasicGame.Intro.prototype = {

    create: function () {
        

        //MUSIC
        this.music = this.add.audio('titleMusic');
        //this.music.play();

        //IMAGES
        this.add.sprite(0, 0, 'sky');

        //  The platforms group contains the ground and the 2 ledges we can jump on
        platforms = this.add.group();
        var ground = platforms.create(0, this.world.height - 96, 'ground');
        ground.scale.setTo(2, 3);
        var ledge = platforms.create(400, 400, 'ground');
        ledge = platforms.create(-150, 250, 'ground');

        // The player and its settings
        this.player = this.add.sprite(32, this.world.height - 144, 'player');
        this.player.animations.add('left', [0, 1, 2, 3], 10, true);
        this.player.animations.add('right', [5, 6, 7, 8], 10, true);



        this.graphics = this.add.graphics(100,100);
        this.graphics.lineStyle(0);
        this.graphics.beginFill(0x000000, 0.8);
        this.graphics.drawEllipse(270, 160, 300, 150);
        this.graphics.endFill();
        this.graphics.visible = false;

        //TIME AND TEXT
        this.timeCheck = this.time.now;
        this.playerSprite = this.add.sprite(300, 206, 'playerSprite');
        this.textShow = this.add.text(200, 200, "", this.style);
        this.playerSprite.visible = false;
        this.textShow.visible = false;
        
        
    },

    update: function () {
        switch(this.flag){
            case 0:
                if(this.player.x < 200){
                    this.player.x += 1;
                    this.player.animations.play('right');
                }else{
                    this.player.animations.stop();
                    this.player.frame = 4;
                    this.flag = 1;
                }
                break;
                
                
            case 1:
                this.graphics.visible = true;
                this.playerSprite.visible = true;
                this.textShow.visible = true;

                if (this.time.now - this.timeCheck > 10){
                    if ( this.i < this.dialogue.length){
                        if (this.j < this.dialogue[this.i].length){
                            this.timeCheck = this.time.now;
                            this.textShow.text += this.dialogue[this.i][this.j];
                            this.j++;
                        }
                    }
                }     

                this.checkMouse();

                if(this.i == this.dialogue.length){
                    this.flag = 2; 
                }
                break;
                
            case 2:
                this.playerSprite.destroy();
                this.textShow.text = 'That is all, folks.';
                this.flag = 3;
                break;
                
            case 3:
                this.state.start('Game');
                break;
                
        }
    },

    checkMouse: function(){
        if (this.input.activePointer.isDown) {
            if (!this.mouseTouchDown)
                this.touchDown();
        } else {
            if (this.mouseTouchDown) 
                this.touchUp();
        }
    },

    touchDown: function(){
        this.mouseTouchDown = true;
        this.i++;
        this.j = 0;
        this.textShow.text = "";
    },


    touchUp: function(){
        this.mouseTouchDown = false;
    },


    quitGame: function (pointer) {

        //  Here you should destroy anything you no longer need.
        //  Stop music, delete sprites, purge caches, free resources, all that good stuff.

        //  Then let's go back to the main menu.
        this.state.start('Game');

    }

};
