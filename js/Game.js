
BasicGame.Game = function (game) {

    //  When a State is added to Phaser it automatically has the following properties set on it, even if they already exist:

    this.game;      //  a reference to the currently running game (Phaser.Game)
    this.add;       //  used to add sprites, text, groups, etc (Phaser.GameObjectFactory)
    this.camera;    //  a reference to the game camera (Phaser.Camera)
    this.cache;     //  the game cache (Phaser.Cache)
    this.input;     //  the global input manager. You can access this.input.keyboard, this.input.mouse, as well from it. (Phaser.Input)
    this.load;      //  for preloading assets (Phaser.Loader)
    this.math;      //  lots of useful common math operations (Phaser.Math)
    this.sound;     //  the sound manager - add a sound, play one, set-up markers, etc (Phaser.SoundManager)
    this.stage;     //  the game stage (Phaser.Stage)
    this.time;      //  the clock (Phaser.Time)
    this.tweens;    //  the tween manager (Phaser.TweenManager)
    this.state;     //  the state manager (Phaser.StateManager)
    this.world;     //  the game world (Phaser.World)
    this.particles; //  the particle manager (Phaser.Particles)
    this.physics;   //  the physics manager (Phaser.Physics)
    this.rnd;       //  the repeatable random number generator (Phaser.RandomDataGenerator)

    //  You can use any of these from any function within this State.
    //  But do consider them as being 'reserved words', i.e. don't create a property for your own game called "world" or you'll over-write the world reference.

    this.dialogue = ["Click to go to the next sentence.", 
        "Look at what I'm doing instead of\nstudying for exam.",
        "But umm... Study hard, play hard?\n(Although I haven't been doing the\nformer.)",
        "Geez, I can't wait for school to be over.",
        "Lol ok, I need to study now.\nCiao!",
        "The game will start after this.\nIt's barely anything, though."];
    this.mouseTouchDown = false;
    this.mouseTouchUp = false;
    this.i = 0;
    this.j = 0;
    this.timeCheck = 0;
    this.style = { font: "20px Verdana", fill: "#fff"};

    this.music = null;
    this.flag = 0;
    this.flag1Create = 0;
    this.flag5Create = 0;


};

BasicGame.Game.prototype = {

    create: function () {
        this.flag = 0;
        
        this.physics.startSystem(Phaser.Physics.ARCADE);
        this.world.setBounds(0, 0, 2100, 600);
        
        //IMAGES
        this.add.sprite(0, 0, 'sky');
        starfield = this.add.tileSprite(0, 0, 2100, 600, 'starfield');
        this.map = this.add.tilemap('map');
        this.map.addTilesetImage('tiles', 'gameTiles');
        this.layer = this.map.createLayer('layer1');
        this.map.setCollisionBetween(1, 1000, true, 'layer1');

        
        // The player and its settings
        this.player = this.add.sprite(50, this.world.height - 166, 'player');
        this.player.animations.add('left', [0, 1, 2, 3], 10, true);
        this.player.animations.add('right', [5, 6, 7, 8], 10, true);
        this.player.anchor.setTo(0.5,0.5);
        
        this.physics.enable(this.player);
        this.camera.follow(this.player);

        this.player.body.bounce.y = 0.2;
        this.player.body.gravity.y = 2000;
        this.player.body.collideWorldBounds = true;

        //Dude
        this.dude = this.add.sprite(400, this.world.height - 141, 'dude');
        this.dude.animations.add('left', [0, 1], 10, true);
        this.dude.animations.add('right', [2,3], 10, true);
        this.physics.enable(this.dude);
        
        //CHANGE THIS AS SPRITE
        this.graphics = this.add.graphics(100,100);
        this.graphics.lineStyle(0);
        this.graphics.beginFill(0x000000, 0.8);
        this.graphics.drawEllipse(270, 160, 300, 150);
        this.graphics.endFill();
        this.graphics.visible = false;

        this.gameoverText = this.add.text(this.world.centerX, this.world.centerY, "", { font: "40px Verdana", fill: "#fff"});
        this.restartText = this.add.text(this.world.centerX, this.world.centerY, "", this.style);
        //this.gameoverText.anchor.x = Math.round(this.gameoverText.width * 0.5) / this.gameoverText.width;
        this.gameoverText.anchor.set(0.5);
        this.restartText.anchor.set(0.5);
        this.playerSprite = this.add.sprite(this.camera.x+300, this.camera.y+206, 'playerSprite');
        this.textShow = this.add.text(this.camera.x+200, this.camera.y+200, "", this.style);
        this.playerSprite.visible = false;
        
        this.add.text(1700, 270, "Congrats, you reach the end.", this.style);
        this.cursors = this.input.keyboard.createCursorKeys();
        this.camera.follow(this.player, Phaser.Camera.FOLLOW_LOCKON);
        
        this.debugText = this.add.text(0, 0, "", this.style);
        

    },

    update: function () {
        this.physics.arcade.collide(this.player, this.layer);
//        this.debugText.position.x = this.player.x;
//        this.debugText.position.y = this.player.y - 40;
//        this.debugText.text = this.flag;
        
        switch(this.flag){
            case 0:
                if(this.player.x < 200){
                    this.player.x += 1;
                    this.player.animations.play('right');
                }else{
                    this.player.animations.stop();
                    this.player.frame = 4;
                    this.flag = 1;
                }
                break;
                
                
            case 1:
                if(this.flag1Create == 0){
                    //only executed once
                    this.graphics.visible = true;
                    this.showPlayerSprite();
                    this.flag1Create = 1;
                }else{
                    //increment text one by one
                    if (this.time.now - this.timeCheck > 9){
                        if ( this.i < this.dialogue.length){
                            if (this.j < this.dialogue[this.i].length){
                                this.timeCheck = this.time.now;
                                this.textShow.text += this.dialogue[this.i][this.j];
                                this.j++;
                            }
                        }
                    }     

                    this.checkMouse();

                    if(this.i == this.dialogue.length){
                        this.flag = 2; 
                    }
                }
                
                
                break;
                
            /***************** MAIN GAME *******************/
            case 2:
                this.graphics.visible = false;
                this.playerSprite.visible = false;
                this.textShow.visible = false;
                
                this.player.body.velocity.x = 0;
                
                if(this.dude.position.x > 1960){
                    this.dude.visible = false;
                }else if(this.dude.position.x - this.player.position.x < 80){
                    //this.dude.position.x += 12;
                    this.timeCheck = this.time.now;
                    this.flag++;
                }
                else{
                    this.dude.position.x += 1.5;
                    this.dude.animations.play('right');
                } 

                if (this.cursors.left.isDown){
                    this.player.body.velocity.x = -200;
                    this.player.animations.play('left');
                }
                else if (this.cursors.right.isDown){
                        this.player.body.velocity.x = 200;
                        this.player.animations.play('right');
                }
                else{
                    this.player.animations.stop();
                    this.player.frame = 4;
                }

                //  Jump
                if (this.cursors.up.isDown && this.player.body.blocked.down){
                    this.player.body.velocity.y = -1000;
                }
                break;
                
                
                
            case 3:
                this.player.body.velocity.x = 0;
                this.player.animations.stop();
                this.dude.animations.stop();
                this.dude.body.velocity.x = 0;
            
                //show
                this.playerSprite.position.x = this.camera.x+300;
                this.playerSprite.position.y = this.camera.y + 204;
                this.playerSprite.visible = true;
                this.textShow.position.x = this.camera.x+200;
                this.textShow.position.y = this.camera.y+200;
                this.textShow.visible = true;
                this.textShow.text = "Dude, you'll scare him.";
                
                this.graphics.position.x = this.camera.x+100;
                this.graphics.position.y = this.camera.y+100;
                this.graphics.visible = true;
                
                this.flag = 10;
                
                this.timer = this.time.events.add(Phaser.Timer.SECOND * 2, function(){this.flag = 4}, this);

                break;
                
            case 4:
                this.playerSprite.visible = false;
                this.textShow.visible = false;
                this.graphics.visible = false
                this.dude.body.velocity.x = 700;
                this.flag++;
                break;
                
            case 5:
                if(this.dude.position.x > this.camera.x+800){
                    this.gameoverText.position.x = this.camera.x+400;
                    this.gameoverText.position.y = this.camera.y+250;
                    this.gameoverText.text = "GAME OVER"
                    this.restartText.position.x = this.camera.x+400;
                    this.restartText.position.y = this.camera.y+300;
                    this.restartText.text = "Click to restart."
                    this.gameoverText.visible = true;
                    this.restartText.visible = true;
                    
                    if(this.input.activePointer.isDown){
                        this.quitGame();
                    }
                }
                
                break;
        }       
        
    },
    
    showPlayerSprite: function(){
        this.playerSprite.position.x = this.camera.x+300;
        this.playerSprite.position.y = this.camera.y+206;
        this.textShow.position.x = this.camera.x+200;
        this.textShow.position.y = this.camera.y+200;
        this.playerSprite.visible = true;
        this.textShow.visible = true;
    },
    
    checkMouse: function(){
        if (this.input.activePointer.isDown) {
            if (!this.mouseTouchDown)
                this.touchDown();
        } else {
            if (this.mouseTouchDown) 
                this.touchUp();
        }
    },

    touchDown: function(){
        this.mouseTouchDown = true;
        this.i++;
        this.j = 0;
        this.textShow.text = "";
    },


    touchUp: function(){
        this.mouseTouchDown = false;
    },

    endGame: function(){
        
    },

    quitGame: function (pointer) {

        //  Here you should destroy anything you no longer need.
        //  Stop music, delete sprites, purge caches, free resources, all that good stuff.
        //  Then let's go back to the main menu.
        
        
        //this.state.start('Boot');
        this.gameoverText.visible = false;
        this.restartText.visible = false;
        this.player.position.x = 200;
        this.player.position.y = this.world.height - 166;
        this.dude.position.x = 400;
        this.dude.position.y = this.world.height-141;
        this.dude.visible = true;
        this.dude.body.velocity.x = 0;
        //this.timer.destroy();
        this.flag = 2;

    }

};
